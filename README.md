The overarching task is the creation of a statistical model for loan default. The risk of a loan application is modeled using binomial regression within the generalised linear modelling framework. This model assigned probability of default to loan applications which allows effective management of a lending (credit risk) portfolio. 

Check out the pdf file for the full report